<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en_EN">

<head>
    <?php include_once('php/head.php') ?>
</head>

<body>

<div id="load">
    <div id="loading">
        <div class="ct--load"></div>
        <div class="ct--load"></div>
        <div class="ct--load"></div>
        <div class="ct--load"></div>
        <div class="ct--load"></div>
        <div class="ct--load"></div>
    </div>
</div>


<div id="site_content">

    <!-- MENU -->
    <div id="menu">

        <div id="mobileMenu">
            <div class="mobileMenuImg">
                <svg class="arrow --svg" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <g transform="matrix(-0.999999,-0.00141153,0.00141153,-0.999999,23.983,24.0169)">
                        <path d="M8.122,24L4,20L12,12L4,4L8.122,0L20,12L8.122,24Z" style="fill-rule:nonzero;"></path>
                    </g>
                </svg>

            </div>
            <div class="mobileMenuImg">
                <svg class="burger --svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M24 6h-24v-4h24v4zm0 4h-24v4h24v-4zm0 8h-24v4h24v-4z"></path>
                </svg>
            </div>
        </div>

        <div id="logo">
            <img src="asset/logo/logo_white.png" alt="logo" class="logoImg"/>
        </div>





        <div class="view--mob">
            <div class="mob">
                <div class="menu_content">
                    <a href="https://www.facebook.com/tetuna/" class="nav facebook">
                        <svg class="--svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path d="M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z"></path>
                        </svg>
                    </a>
                </div>

                <div class="menu_content">
                    <a href="https://soundcloud.com/tetuna" class="nav soundcloud">
                        <svg class="--svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path d="M7 17.939h-1v-8.068c.308-.231.639-.429 1-.566v8.634zm3 0h1v-9.224c-.229.265-.443.548-.621.857l-.379-.184v8.551zm-2 0h1v-8.848c-.508-.079-.623-.05-1-.01v8.858zm-4 0h1v-7.02c-.312.458-.555.971-.692 1.535l-.308-.182v5.667zm-3-5.25c-.606.547-1 1.354-1 2.268 0 .914.394 1.721 1 2.268v-4.536zm18.879-.671c-.204-2.837-2.404-5.079-5.117-5.079-1.022 0-1.964.328-2.762.877v10.123h9.089c1.607 0 2.911-1.393 2.911-3.106 0-2.233-2.168-3.772-4.121-2.815zm-16.879-.027c-.302-.024-.526-.03-1 .122v5.689c.446.143.636.138 1 .138v-5.949z"></path>
                        </svg>
                    </a>
                </div>
            </div>

            <div class="mob">
                <div class="menu_content">
                    <a href="https://www.youtube.com/channel/UChLj6UgmBz_CgDrud3q3t8Q" class="nav youtube">
                        <svg class="--svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path d="M19.615 3.184c-3.604-.246-11.631-.245-15.23 0-3.897.266-4.356 2.62-4.385 8.816.029 6.185.484 8.549 4.385 8.816 3.6.245 11.626.246 15.23 0 3.897-.266 4.356-2.62 4.385-8.816-.029-6.185-.484-8.549-4.385-8.816zm-10.615 12.816v-8l8 3.993-8 4.007z"></path>
                        </svg>
                    </a>
                </div>

                <div class="menu_content">
                    <div class="nav gallery">
                        <svg class="--svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path d="M5 8.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5zm9 .5l-2.519 4-2.481-1.96-4 5.96h14l-5-8zm8-4v14h-20v-14h20zm2-2h-24v18h24v-18z"></path>
                        </svg>
                    </div>
                </div>
            </div>
        </div>







        <div id="socialMenu">
            <div class="menu_content">
                <div class="nav about">
                    <span class="nm">about</span>
                    <svg class="--svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 22c-3.123 0-5.914-1.441-7.749-3.69.259-.588.783-.995 1.867-1.246 2.244-.518 4.459-.981 3.393-2.945-3.155-5.82-.899-9.119 2.489-9.119 3.322 0 5.634 3.177 2.489 9.119-1.035 1.952 1.1 2.416 3.393 2.945 1.082.25 1.61.655 1.871 1.241-1.836 2.253-4.628 3.695-7.753 3.695z"></path>
                    </svg>
                </div>
            </div>

            <div class="menu_content">
                <a href="https://www.facebook.com/tetuna/" class="nav facebook">
                    <span class="nm">Facebook</span>
                    <svg class="--svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path d="M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z"></path>
                    </svg>
                </a>
            </div>

            <div class="menu_content">
                <a href="https://soundcloud.com/tetuna" class="nav soundcloud">
                    <span class="nm">SoundCloud</span>
                    <svg class="--svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path d="M7 17.939h-1v-8.068c.308-.231.639-.429 1-.566v8.634zm3 0h1v-9.224c-.229.265-.443.548-.621.857l-.379-.184v8.551zm-2 0h1v-8.848c-.508-.079-.623-.05-1-.01v8.858zm-4 0h1v-7.02c-.312.458-.555.971-.692 1.535l-.308-.182v5.667zm-3-5.25c-.606.547-1 1.354-1 2.268 0 .914.394 1.721 1 2.268v-4.536zm18.879-.671c-.204-2.837-2.404-5.079-5.117-5.079-1.022 0-1.964.328-2.762.877v10.123h9.089c1.607 0 2.911-1.393 2.911-3.106 0-2.233-2.168-3.772-4.121-2.815zm-16.879-.027c-.302-.024-.526-.03-1 .122v5.689c.446.143.636.138 1 .138v-5.949z"></path>
                    </svg>
                </a>
            </div>

            <div class="menu_content">
                <a href="https://www.youtube.com/channel/UChLj6UgmBz_CgDrud3q3t8Q" class="nav youtube">
                    <span class="nm">Youtube</span>
                    <svg class="--svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path d="M19.615 3.184c-3.604-.246-11.631-.245-15.23 0-3.897.266-4.356 2.62-4.385 8.816.029 6.185.484 8.549 4.385 8.816 3.6.245 11.626.246 15.23 0 3.897-.266 4.356-2.62 4.385-8.816-.029-6.185-.484-8.549-4.385-8.816zm-10.615 12.816v-8l8 3.993-8 4.007z"></path>
                    </svg>
                </a>
            </div>

            <div class="menu_content">
                <div class="nav gallery">
                    <span class="nm">Gallery</span>
                    <svg class="--svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path d="M5 8.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5zm9 .5l-2.519 4-2.481-1.96-4 5.96h14l-5-8zm8-4v14h-20v-14h20zm2-2h-24v18h24v-18z"></path>
                    </svg>
                </div>
            </div>

            <div class="menu_content">
                <div class="nav release">
                    <span class="nm">Release</span>
                    <svg class="--svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path d="M12 8c-2.209 0-4 1.791-4 4s1.791 4 4 4 4-1.791 4-4-1.791-4-4-4zm0 6c-1.104 0-2-.897-2-2s.896-2 2-2 2 .897 2 2-.896 2-2 2zm0-12c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-2.914 20.526c-2.625-.902-4.697-2.978-5.592-5.606l1.02-.127c.807 2.174 2.529 3.901 4.699 4.714l-.127 1.019zm.258-2.054c-1.723-.71-3.098-2.085-3.807-3.807l1.041-.13c.596 1.272 1.623 2.299 2.895 2.896l-.129 1.041zm8.095-9.007c-.598-1.272-1.625-2.3-2.896-2.896l.131-1.041c1.721.71 3.096 2.085 3.807 3.807l-1.042.13zm2.049-.256c-.814-2.157-2.529-3.869-4.691-4.677l.129-1.019c2.613.896 4.68 2.958 5.582 5.568l-1.02.128z"></path>
                    </svg>
                </div>
            </div>

            <div class="menu_content">
                <div class="nav booking">
                    <span class="nm">Booking</span>
                    <svg class="--svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path d="M19 9.062s-5.188-.333-7 1.938c2-4.896 7-5.938 7-5.938v-2l5 4-5 4.019v-2.019zm-18.974 14.938h23.947l-11.973-11.607-11.974 11.607zm1.673-14l10.291-7.488 3.053 2.218c.712-.459 1.391-.805 1.953-1.054l-5.006-3.637-11.99 8.725v12.476l7.352-7.127-5.653-4.113zm15.753 4.892l6.548 6.348v-11.612l-6.548 5.264z"></path>
                    </svg>
                </div>
            </div>
        </div>

    </div>

    <!--MOBILE MENU-->
    <?php include_once ('php/mobileMenu.php') ?>

    <!-- ABOUT -->
    <?php include_once('php/about.php') ?>

    <!-- PICTURES -->
    <?php include_once('php/gallery.php') ?>

    <!-- RELEASE MODULE -->
    <?php include_once('php/release.php') ?>

    <!-- FORM MODULE -->
    <?php include_once('php/booking.php') ?>

    <!-- FOOTER -->
    <footer id="footer">
        <div class="footerElt">
            All right reserved &copy; Tetuna 2018
        </div>

        <div class="footerElt">

            <div class="footerLink">
                Photo by <a href="https://www.facebook.com/batvision/" target="_blank">Batvision</a>
            </div>

            <div class="footerLink">
                Development by <a href="http://aina-ravinala.com/" target="_blank">Aïna</a>
            </div>

        </div>
    </footer>

</div>


<script src="js/script.js"></script>

</body>
</html>