
/*LOAD OVERLAY*/

$(window).on('load', function () {

    setTimeout(function () {
        $('#site_content').css("display", "initial");
        $('#load').fadeOut("fast");
    }, 1000)

});



/*MENU CLICK*/

$('.closeBtn').on('click', function () {
    $('.overlay').fadeOut("fast");
});

$('.about').on('click', function () {
    $('#aboutMeModule').fadeIn("fast");
});

$('.gallery').on('click', function () {
    $('#galleryModule').fadeIn("fast");
});

$('.release').on('click', function () {
    $('#releaseModule').fadeIn("fast");
});

$('.booking').on('click', function () {
    $('#formModule').fadeIn("fast");
});



/*MENU CLICK MOBILE*/

$('.arrowImg').on('click', function () {
    $('.overlay').fadeOut("fast");
});

$('.burgerMenuImg').on('click', function () {
    $('.overlay').fadeOut("fast");
    $('#mobileMenuModule').fadeIn("fast");
    $('.arrowImg').css('display', 'block');
});







/*CONTACT FORM VALIDATE*/

$('#g-form').on('submit', function (e) {

    var $gForm = $('#g-form');
    var $msg = $('#msg');

    $('document').css('cursor', 'wait');
    $gForm.css('cursor', 'wait');

    $.ajax({
        type: "POST",
        url: "../php/send-config.php",
        data: $gForm.serialize(),
        success: function (result) {

            if (result) {
                var $sendMsgV = $('.send-msg-v');
                $('document').css('cursor', 'initial');
                $gForm.css('cursor', 'initial');
                $msg.css('height', '59px');
                $gForm[0].reset();
                $gForm.animate({scrollTop: $gForm.offset().top}, 'fast');
                $sendMsgV.html('Thank\'s for your interest, your email has been sent.');
                $sendMsgV.fadeIn("slow");

                setTimeout(function () {
                    $sendMsgV.fadeOut("slow");
                }, 5000);

                setTimeout(function () {
                    $msg.css('height', '0');
                }, 5500);

            } else {
                var $sendMsgN = $('.send-msg-n');
                $('document').css('cursor', 'initial');
                $gForm.css('cursor', 'initial');
                $msg.css('height', '59px');
                $gForm.animate({scrollTop: $gForm.offset().top}, 'fast');
                $sendMsgN.html('Sorry, your email has been not sent,' +
                    ' please retry later or sent a mail at tetunamusic@gmail.com');
                $sendMsgN.fadeIn("slow");

                setTimeout(function () {
                    $sendMsgN.fadeOut("slow");
                }, 10000);

                setTimeout(function () {
                    $msg.css('height', '0');
                }, 10500);
            }
        },
    });
    e.preventDefault();
});


<<<<<<< HEAD
=======



/*Remplacement des images svg par les data*/

$(function() {
    $('img.--svg').each(function() {
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');
// Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }
// Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
// Replace image with new SVG
            $img.replaceWith($svg);
        }, 'xml');
    });
});

$('svg.arrowImg').each(function () {
   $(this).css('width', '30px');
   $(this).css('height', '30px');
});

$('svg.burgerMenuImg').each(function () {
    $(this).css('width', '30px');
    $(this).css('height', '30px');
});
>>>>>>> master
