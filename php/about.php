<div id="aboutMeModule" class="overlay">
    <div class="close">
        <img src="asset/wp-content/cross.svg" alt="Close" class="closeBtn  --svg" />
    </div>

    <div class="module">
        <div class="about_me">
            <div class="logoAbout">
                <img src="asset/logo/logo_white.png" alt="logo" class="logoImgAbout" />
            </div>
            <h3>ABOUT</h3>
            <p>It was in 1998 that Tetuna discovered Psytrance thanks to a track from GMS heard on "Radio Pulsar",
                a small student radio association. Since then his view of music has changed, and Psy-Trance became
                part of his universe.</p>

            <p>For five years, he performed as a DJ on various local scenes and since 2003, he has been developing
                his own musical universe, oscillating between full-on, morning and psy-trance, influenced by artists
                such as Bamboo Forest, Sonicsurfeur, Silicon Sound, Toires, Nomad, Neuromotor, Total Eclipse or Absolum.</p>

            <p>He produced his first album Psychedelic Transition in 2009, as well as some EPs with Goa Records.
                He kept on working by producing in 2012, a second album - Crazy Trips - with GeomagneticTV,
                with which he also released his latest album - Alien Existence - in 2016.</p>

            <p>He performs on stages such as the Openmind Festival in Tours (Fr) or the Lunatic Asylum in Lyon (Fr).</p>

            <p>His creativity and boundless energy make him an accomplished artist, ready to make the crowds
                dance on all dancefloors.</p>










        </div>
    </div>
</div>