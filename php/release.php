<div id="releaseModule" class="overlay">

    <div class="close">
        <img src="asset/wp-content/cross.svg" alt="Close" class="closeBtn --svg" />
    </div>

    <div class="module">

        <h2>LAST PREVIEW</h2>
        <div class="ligne"></div>
        <iframe scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/559183557&color=%23808080&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>

        <h2>ALBUM</h2>
        <div class="ligne"></div>

        <div class="album section fadeIn">


            <div class="album_elt elt ">
                <div class="main_cover">
                    <img src="asset/album_cover/album/11_2009_psychedelic_transition_album.jpg" alt="Psychedelic Transition / 2009 - Cover" class="cover">
                </div>
                <span class="title">Tetuna - Psychedelic Transition</span>
                <span class="date">2009</span>
                <a href="https://www.beatport.com/release/psychedelic-transition/178336" class="store">Beatport</a>
                <a href="https://open.spotify.com/album/2T1WZbaghX0a8RmLNSE2bA" class="store">Spotify</a>
                <a href="https://itunes.apple.com/us/album/psychedelic-transition/507038200" class="store">Itunes</a>
                <a href="https://goarecords.bandcamp.com/album/tetuna-psychedelic-transition-goarec011-goa-records" class="store">Bandcamp</a>
                <a href="https://www.psyshop.com/Psychedelic-Transition-Goa/gre1cd011/" class="store">Psyshop</a>
                <a href="https://www.deezer.com/en/album/1653477" class="store">Deezer</a>
                <a href="http://www.goastore.com/tetuna-psychedelic-transition.html" class="store">Goastore</a>
                <a href="https://www.amazon.fr/Psychedelic-Transition-Tetuna/dp/B002U9IMOA/ref=sr_1_2?s=music&ie=UTF8&qid=1526563730&sr=1-2&keywords=tetuna" class="store">Amazon</a>
            </div>

            <div class="album_elt elt">
                <div class="main_cover">
                    <img src="asset/album_cover/album/02_2012_crazy_trips_album.jpg" alt="Crazy Trips / 2012 - Cover" class="cover">
                </div>
                <span class="title">Tetuna - Crazy Trips</span>
                <span class="date">2012</span>
                <a href="https://www.beatport.com/release/crazy-trips/849566" class="store">Beatport</a>
                <a href="https://open.spotify.com/album/43TDIh20YPWy8GDbDAKGWR" class="store">Spotify</a>
                <a href="https://itunes.apple.com/fr/album/crazy-trips/488933204" class="store">Itunes</a>
                <a href="https://goarecords.bandcamp.com/album/tetuna-crazy-trips-goarec018-goa-records-geomagnetic-tv" class="store">Bandcamp</a>
                <a href="https://www.psyshop.com/Crazy-Trips-Tetuna-Goa/gre1cd018/" class="store">Psyshop</a>
                <a href="https://www.junodownload.com/products/te-tuna-crazy-trips/1880967-02/" class="store">Juno Download</a>
                <a href="https://www.deezer.com/en/album/6235550" class="store">Deezer</a>
                <a href="http://www.goastore.com/tetuna-crazy-trips.html" class="store">Goastore</a>
                <a href="https://www.amazon.fr/Crazy-Trips-TeTuna/dp/B00B1L051Q/ref=sr_1_cc_1?s=aps&ie=UTF8&qid=1526563879&sr=1-1-catcorr&keywords=tetuna+crazy+trips" class="store">Amazon</a>
            </div>

            <div class="album_elt elt">
                <div class="main_cover">
                    <img src="asset/album_cover/album/07_2016_alien_existence_album.jpg" alt="Alien Existence / 2016 - Cover" class="cover">
                </div>
                <span class="title">Tetuna - Alien Existence</span>
                <span class="date">2016</span>
                <a href="https://www.beatport.com/release/alien-existence/1805000" class="store">Beatport</a>
                <a href="https://open.spotify.com/album/2NiGL2C7a3e7fzfj1ljeDi" class="store">Spotify</a>
                <a href="https://itunes.apple.com/fr/album/alien-existence/1128967421" class="store">Itunes</a>
                <a href="https://geomagneticrecords.bandcamp.com/album/tetuna-alien-existence-geocd116-geomagnetic-" class="store">Bandcamp</a>
                <a href="https://www.psyshop.com/Alien-Existence-Tetuna/geo1cd116/" class="store">Psyshop</a>
                <a href="https://www.junodownload.com/products/tetuna-alien-existence/3148130-02/" class="store">Juno Download</a>
                <a href="https://www.deezer.com/en/album/13462721" class="store">Deezer</a>
                <a href="http://www.goastore.com/tetuna-alien-existence.html" class="store">Goastore</a>
                <a href="https://www.amazon.fr/Alien-Existence-allemand-Tetuna/dp/B01HNC5F7U" class="store">Amazon</a>
            </div>

        </div>


        <h2>EP</h2>
        <div class="ligne"></div>

        <div class="ep section fadeIn">



            <div class="ep_elt elt">
                <div class="main_cover">
                    <img src="asset/album_cover/ep/05_2009_seeds_EP.jpg" alt="Seeds / 2009 - Cover" class="cover">
                </div>
                <span class="title">Tetuna - Seeds</span>
                <span class="date">2009</span>
                <a href="https://www.beatport.com/release/seeds-ep/180334" class="store">Beatport</a>
                <a href="https://open.spotify.com/album/7E49kKy9kVKKd2EOaqv5gw" class="store">Spotify</a>
                <a href="https://goarecords.bandcamp.com/album/tetuna-seeds-ep" class="store">Bandcamp</a>
            </div>

            <div class="ep_elt elt">
                <div class="main_cover">
                    <img src="asset/album_cover/ep/05_2009_TuneIn_EP.jpg" alt="Tune In / 2009 - Cover" class="cover">
                </div>
                <span class="title">Tetuna - Tune In</span>
                <span class="date">2009</span>
                <a href="https://www.beatport.com/release/tune-in-ep/186493" class="store">Beatport</a>
                <a href="https://open.spotify.com/album/2DH04Bk7NRRPJVkwUP293Y" class="store">Spotify</a>
                <a href="https://goarecords.bandcamp.com/album/te-tuna-tune-in-ep" class="store">Bandcamp</a>
                <a href="https://www.junodownload.com/products/tetuna-tune-in-ep/1442543-02/" class="store">Juno Download</a>
            </div>

            <div class="ep_elt elt">
                <div class="main_cover">
                    <img src="asset/album_cover/ep/08_2009_Atmotrips_EP.jpg" alt="Atmotrips / 2009 - Cover" class="cover">
                </div>
                <span class="title">Tetuna - Atmotrips</span>
                <span class="date">2009</span>
                <a href="https://www.beatport.com/release/atmotrips-ep/178150" class="store">Beatport</a>
                <a href="https://itunes.apple.com/fr/album/atmotrips-ep/320605745" class="store">Itunes</a>
                <a href="https://beatspace-geomagnetic.bandcamp.com/album/tetuna-atmotrips-digital-ep-digital-drugs-coalition" class="store">Bandcamp</a>
                <a href="https://www.junodownload.com/products/tetuna-atmotrips-ep/1438575-02/" class="store">Juno Download</a>
                <a href="https://www.deezer.com/en/album/579335" class="store">Deezer</a>
                <a href="https://www.amazon.fr/Atmotrips-EP-TeTuna/dp/B002EA690Y/ref=sr_1_9?s=music&ie=UTF8&qid=1526563940&sr=8-9&keywords=tetuna+atmotrips" class="store">Amazon</a>
            </div>

            <div class="ep_elt elt">
                <div class="main_cover">
                    <img src="asset/album_cover/ep/03_2012_eve_dreams_ep.jpg" alt="Eve Dreams / 2012 - Cover" class="cover">
                </div>
                <span class="title">Tetuna - Eve Dreams</span>
                <span class="date">2012</span>
                <a href="https://www.beatport.com/release/eve-dreams-ep/872213" class="store">Beatport</a>
                <a href="https://open.spotify.com/album/38BahDUefhBqotciyz3pSI" class="store">Spotify</a>
                <a href="https://itunes.apple.com/fr/album/eve-dreams-ep-single/502723784" class="store">Itunes</a>
                <a href="https://geomagneticrecords.bandcamp.com/album/tetuna-eve-dreams-ep" class="store">Bandcamp</a>
                <a href="https://www.junodownload.com/products/tetuna-eve-dreams-ep-lite-edition/1914242-02/" class="store">Juno Download</a>
                <a href="https://www.deezer.com/en/album/1551803" class="store">Deezer</a>
                <a href="https://www.amazon.fr/Tetuna-Eve-Dreams-EP-TeTuna/dp/B0079WHTSS/ref=sr_1_1?s=music&ie=UTF8&qid=1526563841&sr=8-1&keywords=tetuna+eve+dreams" class="store">Amazon</a>
            </div>

            <div class="ep_elt elt">
                <div class="main_cover">
                    <img src="asset/album_cover/ep/12_2017_human_curiosity_track.png" alt="Human Curiosity / 2017 - Cover" class="cover">
                </div>
                <span class="title">Tetuna, 01-N - Human Curiosity</span>
                <span class="date">2017</span>
                <a href="https://www.beatport.com/track/human-curiosity-original-mix/8267853" class="store">Beatport</a>
                <a href="https://open.spotify.com/album/3pzairma7KeBo1jEZipGRY" class="store">Spotify</a>
                <a href="https://itunes.apple.com/us/album/human-curiosity-single/1315900084" class="store">Itunes</a>
                <a href="https://geomagneticrecords.bandcamp.com/album/01-n-tetuna-human-curiosity-geoep275-geomagnetic" class="store">Bandcamp</a>
                <a href="https://www.psyshop.com/Human-Curiosity-01N-Tetuna/geo1dw275/" class="store">Psyshop</a>
                <a href="https://www.junodownload.com/products/01-n-tetuna-human-curiosity/3602490-02/" class="store">Juno Download</a>
                <a href="https://www.deezer.com/en/album/52057492" class="store">Deezer</a>
                <a href="http://www.goastore.com/01-n-and-tetuna-human-curiosity-ep.html" class="store">Goastore</a>
                <a href="https://www.amazon.co.uk/Human-Curiosity-TeTuna-01-N/dp/B077N9CFTS" class="store">Amazon</a>
            </div>
        </div>


        <h2>Track</h2>
        <div class="ligne"></div>

        <div class="track section fadeIn">



            <div class="track_elt elt">
                <div class="main_cover">
                    <img src="asset/album_cover/track/12_2017_clarity_track.PNG" alt="Clarity / 2017 - Cover" class="cover">
                </div>
                <span class="title">Tetuna vs Mr.Ankh - Clarity</span>
                <span class="date">2017</span>
                <a href="https://soundcloud.com/tetuna/clarity" class="store">SoundCloud</a>
            </div>

            <div class="track_elt elt">
                <div class="main_cover">
                    <img src="asset/album_cover/track/12_2017_soul_experience_track.jpg" alt="Soul Experience / 2012 - Cover" class="cover">
                </div>
                <span class="title">Tetuna - Soul Experience</span>
                <span class="date">2017</span>
                <a href="https://www.beatport.com/track/soul-experience-original-mix/10024186" class="store">Beatport</a>
                <a href="https://open.spotify.com/album/7n426bcWxclaiJMFpbyR80" class="store">Spotify</a>
                <a href="https://tendancemusicrecords.bandcamp.com/album/tetuna-soul-experience" class="store">Bandcamp</a>
                <a href="https://www.junodownload.com/products/tetuna-soul-experience/3619026-02/" class="store">Juno Download</a>
                <a href="https://itunes.apple.com/ca/album/soul-experience-single/1318568395" class="store">Itunes</a>
                <a href="https://www.deezer.com/en/album/52537922" class="store">Deezer</a>
                <a href="https://www.amazon.fr/Soul-Experience-TeTuna/dp/B077SJ2WMJ" class="store">Amazon</a>
            </div>

            <div class="track_elt elt">
                <div class="main_cover">
                    <img src="asset/album_cover/track/01_2018_inner_call_track.jpg" alt="Inner Call / 2018 - Cover" class="cover">
                </div>
                <span class="title">IKØN & Tetuna - Inner Call</span>
                <span class="date">2018</span>
                <a href="https://soundcloud.com/ikonfr/ikon-tetuna-inner-call-free-download" class="store">SoundCloud</a>
            </div>

            <div class="track_elt elt">
                <div class="main_cover">
                    <img src="asset/album_cover/track/07_2018_celestial.jpg" alt="celestial / 2018 - Cover" class="cover">
                </div>
                <span class="title">Bell Size Park - Welcome To The Freak Show (Feat. Tetuna)</span>
                <span class="date">2018</span>
                <a href="https://www.beatport.com/release/celestial/2304658" class="store">Beatport</a>
                <a href="https://www.youtube.com/watch?v=8cFbJ9jlCMc" class="store">Youtube</a>
            </div>
        </div>

    </div>
</div>