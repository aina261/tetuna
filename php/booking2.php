<div id="formModule" class="overlay">
    <div class="close">
        <img src="asset/wp-content/cross.svg" alt="Close" class="closeBtn --svg" />
    </div>

    <div class="module">
        <div class="contact-form">
            <div id="logoForm">
                <img src="asset/logo/logo_white.png" alt="logo" class="logoImgForm" />
            </div>
            <h3>BOOKING - CONTACT</h3>

            <p>Send your mail at</p>
            <a href="mailto:contact@tetuna.com">contact@tetuna.com</a>
        </div>
    </div>
</div>
