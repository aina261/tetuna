<?php
    $name       =   $_POST['name']; //Nom de l'envoyeur
    $email      =   $_POST['email']; //Email de l'envoyeur
    $phone      =   $_POST['phone']; //Téléphone de l'envoyeur
    $eventDate  =   $_POST['eventDate']; //Date de l'event
    $eventName  =   $_POST['eventName']; //Nom de l'event
    $formSubject=   htmlspecialchars($_POST['formSubject']);
    $subject    =   'Un nouveau contact souhaite vous joindre.'; //Sujet du mail

    $headers    =   'MIME-Version: 1.0'. "\r\n";
    $headers   .=   'Content-type: text/html; charset=UTF-8'. "\r\n";
    $headers   .=   'Bcc:a.ramiarasoa@gmail.com'. "\r\n";

    $img        =   'http://www.tetuna.com/asset/logo/logo_black.png';

    $message    =   '
                    <!-- Emails use the XHTML Strict doctype -->
                    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <!-- The character set should be utf-8 -->
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <meta name="viewport" content="width=device-width"/>
                    </head>
                    <body>
                    <!-- Wrapper for the body of the email -->
                    <table style="margin: auto; color: #000000" width="600px">
                        <tr>
                            <!-- The class, align, and <center> tag center the container -->
                            <td align="center" valign="top">
                                <!-- The content of your email goes here. -->
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="color: #000000">
                                    <tr>
                                        <center>
                                        <img src=" '. $img .' " alt="Logo" width="200px"/></center>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h1 style="text-align: center; color: #000000">Information du contact :</h1>
                                        </td>
                                    </tr>
                                        
                                    <tr>
                                        <td>                                            
                                            <h3 id="name" style="text-align: left; color: #000000">Envoyé par :</h3>
                                            <div style="color: #000000">' .$name. '</div>
                                        </td>
                                    </tr>
                                    
                                    <tr>                                        
                                        <td>                                            
                                            <h3 id="mail" style="text-align: left; color: #000000">Email :</h3>
                                            <div style="color: #000000">' .$email. '</div>
                                        </td>
                                    </tr>
                                    
                                    <tr>                                        
                                        <td>
                                            <h3 id="phone" style="text-align: left; color: #000000">Téléphone :</h3>
                                            <div style="color: #000000">' .$phone. '</div>
                                        </td>
                                    </tr>
                                    
                                    <tr>                                        
                                        <td>
                                            <h3 id="eventDate" style="text-align: left; color: #000000">Date de l\'évenement :</h3 >
                                            <div style="color: #000000">' .$eventDate. '</div>
                                        </td>
                                    </tr>
                                    
                                    <tr>                                        
                                        <td>
                                            <h3 id="eventName" style="text-align: left; color: #000000">Nom  de l\'évenement :</h3 >
                                            <div style="color: #000000">' .$eventName. '</div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <h1 style="text-align: center, color: #000000">Le Message :</h1>
                                            <p style="text-align: left, color: #000000">' . nl2br($formSubject) . '</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </body>
                    </html>
                    ';


    $mail         =   'mplurv@gmail.com'; //destinataire
    /*$mail         =   'tetunamusic@gmail.com'; //destinataire*/


    echo mail($mail, $subject, $message, implode("\r\n", $headers));
