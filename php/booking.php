<div id="formModule" class="overlay">
    <div class="close">
        <img src="asset/wp-content/cross.svg" alt="Close" class="closeBtn  --svg" />
    </div>

    <div class="module">
        <div class="contact-form">
            <div id="logoForm">
                <img src="asset/logo/logo_white.png" alt="logo" class="logoImgForm" />
            </div>
            <h3>BOOKING - CONTACT</h3>

            <div id="msg">
                <p class="send-msg-v"></p>
                <p class="send-msg-n"></p>
            </div>

            <form id="g-form">
                <input name="name" type="text" id="fromName" class="form-input" placeholder="Name *" required />
                <input name="email" type="email" id="formEmail" class="form-input" placeholder="Email *" required />
                <input name="phone" type="tel" id="formPhone" class="form-input" placeholder="International Phone" />
                <input name="eventName" type="text" id="formEventName" class="form-input" placeholder="Event name" />
                <input name="eventDate" type="text" id="formEventDate" class="form-input" placeholder="Event Date" />
                <textarea name="formSubject" id="formSubject" class="form-input msg" rows="10" cols="70" placeholder="Your message *" required></textarea>
                <input type="submit" value="SUBMIT" class="submit-form" />
            </form>
        </div>
    </div>
</div>
