<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122819611-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-122819611-1');
</script>


<meta name="Language" content="en"/>
<meta charset="UTF-8"/>
<meta name="author" content="Aina"/>
<meta name="Copyright" content="Tetuna"/>
<meta name="theme-color" content="#3C3E75"/>
<meta name="description"
      content="Tetuna is a french psytrance artist and live performer artist since 15 years, Tetuna has a unique vision of trance. He discovered Psy-trance music in 1998 and met some famous artists as GMS or Silicon Sound. He started DJ during 5 years and since 2002 and shares his own musical universe, oscillating between full-on, morning and psy-trance."/>
<meta name="keywords"
      content="tetuna, Tetuna, france, full on, progressive, psy, psychedelic trance, psytrance, psytrance france"/>

<meta property="og:locale" content="en_EN"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="TETUNA"/>
<meta property="og:description" content="Tetuna, Psytrance artist and live performer"/>
<meta property="og:image" content="https://tetuna.com/asset/logo/og_logo_black.png"/>
<meta property="og:url" content="https://tetuna.com"/>
<meta property="og:site_name" content="tetuna.com"/>

<meta name="Publisher" content="Tetuna"/>
<meta name="Revisit-After" content="7 days"/>
<meta name="robots" content="index"/>

<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="x-ua-compatible" content="ie=edge"/>

<title>TETUNA | Artist and live performer</title>

<!--<link rel="stylesheet" href="style/style2.css" type="text/css">
<link rel="stylesheet" href="style/responsive.css" type="text/css">
<link rel="stylesheet" href="style/load.css">-->
<link rel="stylesheet" href="style/scss/style.css"/>
<link rel="stylesheet" href="style/scss/mediaQuery.css"/>

<link rel="icon" type="image/png" href="asset/icon/logo.png"/>

<link type="text/plain" rel="author" href="https://tetuna.com/humans.txt"/>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
